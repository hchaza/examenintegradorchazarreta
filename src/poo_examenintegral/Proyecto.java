/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenintegral;

import java.util.List;

/**
 *
 * @author Horacio Chazarreta
 */
public class Proyecto {

    private int id;
    private String nombre;
    private int montoDestinado;
    private List<Empleado> emp;

    public Proyecto() {
    }

    public Proyecto(int id, String nombre, int montoDestinado) {
        this.id = id;
        this.nombre = nombre;
        this.montoDestinado = montoDestinado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMontoDestinado() {
        return montoDestinado;
    }

    public void setMontoDestinado(int montoDestinado) {
        this.montoDestinado = montoDestinado;
    }

    public List<Empleado> getLista() {
        return emp;
    }

    public void setLista(List<Empleado> lista) {
        this.emp = lista;
    }

    @Override
    public String toString() {
        return "Proyecto{" + "id=" + id + ", nombre=" + nombre + ", montoDestinado=" + montoDestinado + '}';
    }

}
