/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenintegral;

import java.util.Comparator;

/**
 *
 * @author Horacio Chazarreta
 */
public class CompararEmpleado implements Comparator<Empleado> {

    @Override
    public int compare(Empleado e1, Empleado e2) {
        int resultado = e1.getApellido().compareTo(e2.getApellido());
        if (resultado != 0) {
            return resultado;
        } else {
            return e1.getNombre().compareTo(e2.getNombre());
        }

    }

}
