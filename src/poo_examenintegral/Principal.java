/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_examenintegral;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Horacio Chazarreta
 */
public class Principal {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean salir = false;
        int opcion, opcion2, opcion3;
        List<Empleado> listaEmp = new ArrayList();
        List<Proyecto> listaProy = new ArrayList();
        Empleado emp1 = new Empleado(23456789, "Perez", "Juan", "38852341", "pjuan@gmail.com");
        listaEmp.add(emp1);
        Proyecto proy1 = new Proyecto(1, "Gestión de Residuos Sólidos", 45000);
        listaProy.add(proy1);
        Proyecto proy2 = new Proyecto();
        //Empleado emp2 = new Empleado();
        int dni, id, monto;
        String apellido, nombre, telefono, email, nombreProy;
        String respuesta;
        //Empleado emp = new Empleado();
        try {
            while (!salir) {

                System.out.println("-------- MENU PRINCIPAL ---------");
                System.out.println("1. Empleados");
                System.out.println("2. Proyectos");
                System.out.println("3. Salir");

                System.out.println("Ingrese una de las opciones");
                opcion = sc.nextInt();

                switch (opcion) {
                    case 1:
                        boolean volver1 = false;

                        while (!volver1) {
                            System.out.println("--------MENU----------------");
                            System.out.println("1. Agregar Empleado");
                            System.out.println("2. Eliminar Empleado");
                            System.out.println("3. Listar Empleados");
                            System.out.println("4. Volver");

                            System.out.println("Ingrese una de las opciones");
                            opcion2 = sc.nextInt();

                            switch (opcion2) {
                                case 1:
                                    // Agregar empleado
                                    do {
                                        System.out.println("Datos del Empleado");
                                        System.out.println("Ingrese DNI: ");
                                        dni = sc.nextInt();
                                        sc.nextLine();
                                        System.out.println("Ingrese Apellido: ");
                                        apellido = sc.nextLine();
                                        System.out.println("Ingrese Nombre: ");
                                        nombre = sc.nextLine();
                                        System.out.println("Ingrese Teléfono: ");
                                        telefono = sc.nextLine();
                                        System.out.println("Ingrese Email: ");
                                        email = sc.nextLine();
                                        Empleado emp = new Empleado();
                                        emp.setDni(dni);
                                        emp.setApellido(apellido);
                                        emp.setNombre(nombre);
                                        emp.setTelefono(telefono);
                                        emp.setEmail(email);
                                        listaEmp.add(emp);
                                        System.out.println("¿Desea Agregar otro Empleado? (Si/No)");
                                        respuesta = sc.nextLine();
                                    } while (respuesta.equalsIgnoreCase("Si"));
                                    break;
                                case 2:
                                    // Eliminar datos de un empleado a partir de su dni
                                    boolean encontradoE = false;
                                    System.out.println("Ingrese DNI del Empleado que desea Eliminar: ");
                                    int dniE = sc.nextInt();
                                    Iterator<Empleado> iterador = listaEmp.iterator();
                                    while (iterador.hasNext()) {
                                        Empleado emp = iterador.next();
                                        if (dniE == emp.getDni()) {
                                            iterador.remove();
                                            System.out.println("El Empleado " + emp.getApellido() + " " + emp.getNombre() + " fue eliminado");
                                            encontradoE = true;
                                        }
                                    }
                                    if (encontradoE == false) {
                                        System.out.println("No se encontró DNI");
                                    }
                                    break;
                                case 3:
                                    // Listar empleados
                                    System.out.println("Listado de todos los Empleados");
                                    Collections.sort(listaEmp, new CompararEmpleado());
                                    for (Empleado emp : listaEmp) {
                                        System.out.println(emp);
                                        
                                    }
                                    System.out.println("La cantidad de empleados es: " + listaEmp.size());
                                    break;
                                case 4:
                                    // Vuelve al menu principal
                                    volver1 = true;
                                    break;
                                default:
                                    System.out.println("Ingrese solo opciones entre 1 y 4");
                            }

                        }

                        break;
                    case 2:
                        boolean volver2 = false;
                        while (!volver2) {
                            System.out.println("--------MENU----------------");
                            System.out.println("1. Agregar un nuevo Proyecto");
                            System.out.println("2. Eliminar un Proyecto");
                            System.out.println("3. Listar Proyectos");
                            System.out.println("4. Agregar Empleado a un Proyecto");
                            System.out.println("5. Quitar Empleado de un Proyecto");
                            System.out.println("6. Listar datos de un Proyecto");
                            System.out.println("7. Calcular total de Montos destinados a Proyectos");
                            System.out.println("8. Volver");
                            System.out.println("Ingrese una de las opciones");
                            opcion3 = sc.nextInt();
                            switch (opcion3) {
                                case 1:
                                    // Permite agregar datos de un nuevo proyecto (sin empleados asignados)
                                    do {
                                        System.out.println("Datos del Proyecto");
                                        System.out.println("Ingrese ID: ");
                                        id = sc.nextInt();
                                        sc.nextLine();
                                        System.out.println("Ingrese Nombre: ");
                                        nombreProy = sc.nextLine();
                                        System.out.println("Ingrese Monto destinado: ");
                                        monto = sc.nextInt();
                                        sc.nextLine();
                                        Proyecto proy = new Proyecto();
                                        proy.setId(id);
                                        proy.setNombre(nombreProy);
                                        proy.setMontoDestinado(monto);
                                        listaProy.add(proy);
                                        System.out.println("¿Desea Agregar otro Proyectoo? (Si/No)");
                                        respuesta = sc.nextLine();
                                    } while (respuesta.equalsIgnoreCase("Si"));
                                    break;
                                case 2:
                                    // Elimina el proyecto a partir de su codigo
                                    boolean encontradoE = false;
                                    System.out.println("Ingrese ID del Proyecto que desea Eliminar: ");
                                    int idE = sc.nextInt();
                                    Iterator<Proyecto> iterador = listaProy.iterator();
                                    while (iterador.hasNext()) {
                                        Proyecto proy = iterador.next();
                                        if (idE == proy.getId()) {
                                            iterador.remove();
                                            System.out.println("El Proyecto " + proy.getNombre() + " fue eliminado");
                                            encontradoE = true;
                                        }
                                    }
                                    if (encontradoE == false) {
                                        System.out.println("No se encontró ID");
                                    }
                                    break;
                                case 3:
                                    // Muestra un listado de todos los proyectos
                                    System.out.println("Listado de todos los Proyectos");
                                    for (Proyecto proy : listaProy) {
                                        System.out.println(proy);
                                        
                                    }
                                    break;
                                case 4:
                                    // Permite asignar un empleado (ya dado de alta previamente) a un determinado proyecto
//                                    listaProy.add(emp);
//                                    for (Proyecto proy : listaProy) {
//                                        listaProy.add((Proyecto) proy2.getLista());
//                                        
//                                    }
                                    break;
                                case 5:
                                    // Permite quitar un empleado (ya dado de alta previamente) a un determinado proyecto

                                    break;
                                case 6:
                                    // A partir de un código de proyecto, se procede a listar todos los que participan del mismo
                                    break;
                                case 7:
                                    // Sumar todos los montos de todos los proyectos
                                    int total = 0;
                                    for (Proyecto proy : listaProy) {
                                        total += proy.getMontoDestinado();
                                    }
                                    System.out.println("El total de montos destinados a Proyectos es: " + total);
                                    break;
                                case 8:
                                    // Vuelve al menu principal
                                    volver2 = true;
                                    break;
                                default:
                                    System.out.println("Solo números entre 1 y 8");
                            }
                        }
                        break;
                    case 3:
                        salir = true;
                        System.out.println("Fin del Programa");
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 3");
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("Ingrese un número");
        }
    }
}
